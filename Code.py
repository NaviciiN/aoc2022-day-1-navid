import tempfile
import unittest

with open(r'C:\Users\Navid\AoC2022\input.txt') as f:
    groups = f.read().strip().split('\n\n')
    max_calories = 0
    for group in groups:
        calories = [int(x) for x in group.split('\n')]
        total_calories = sum(calories)
        if total_calories > max_calories:
            max_calories = total_calories
    print(max_calories)

with open(r'C:\Users\Navid\AoC2022\input.txt') as f:
    elf_calories = []
    total_calories = 0
    
    for line in f:
        line = line.strip()
        if line:
            total_calories += int(line)
        else:
            elf_calories.append(total_calories)
            total_calories = 0
    
    # Append the last Elf's Calories
    elf_calories.append(total_calories)
    
    # Sort the list of all the Elves' Calories in descending order
    elf_calories.sort(reverse=True)
    
    # Sum the top three values
    top_three = sum(elf_calories[:3])

print(top_three)

class TestMaxCalories(unittest.TestCase):
    
    def test_max_calories(self):
        input_str = "200\n300\n400\n\n100\n200\n\n500\n600\n700\n800\n\n"
        expected_output = 2600
        
        with tempfile.NamedTemporaryFile(mode='w', delete=False) as f:
            f.write(input_str)
            f.flush()
            
            with open(f.name, 'r') as input_file:
                groups = input_file.read().strip().split('\n\n')
                max_calories = 0
                for group in groups:
                    calories = [int(x) for x in group.split('\n')]
                    total_calories = sum(calories)
                    if total_calories > max_calories:
                        max_calories = total_calories
                self.assertEqual(max_calories, expected_output)

if __name__ == '__main__':
    unittest.main()
